import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

	public class task02{

		public static void main (String [] args) {
	 
			System.out.print ("������� ����� :"); 
			Scanner inputFigure = new Scanner (System.in); 
			int n = inputFigure.nextInt (); 
			int[] arr = IntStream
			        .iterate(n, i -> i / 10 > 0 || i % 10 > 0, i -> i / 10)
			        .map(i -> i % 10)
			        .toArray();

			int result = 0;

		    for (int i =arr.length -1 , j = 0; i >= 0; --i, j++) {
		        int pos = (int)Math.pow(10, i);
		        result += arr[j] * pos;
		    }
		    System.out.println(result);
		}
	}

